﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LunchApp.Models
{   [Table("Users")]
    public class Users
    {   [Key]
        public int UserID { get; set; }
        public int OrderID { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }

        public List<SelectListItem> GenerateUsers()
        {
            List<SelectListItem> listRandom = new List<SelectListItem>();
            for (int i = 0; i <= 6; i++)
            {
                Users genU = new Users();
                
                genU.UserID = i;
                genU.FirstName = RandomName();
                genU.Lastname = RandomName();
                listRandom.Insert(i, new SelectListItem()
                {
                    Text = genU.FirstName,
                    Value = genU.UserID.ToString()
                });

            }
            return listRandom;


    }
        public string RandomName()
        {
            Random _random = new Random();
            int length = 5;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(_random.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }
    }




    
}
