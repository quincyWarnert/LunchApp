﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LunchApp.Models
{    [Table("Product")]
    public class Products
    {
        [Key]
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }   
        public int Price { get; set; }




        //Create dummy data to display in items list
        public List<Products> foodItems()
        {
            
            Random _random = new Random();
            List<Products> itemList = new List<Products>();
            for (int i = 0; i <= 6; i++)
            {
                Products rProd = new Products();
                rProd.ProductID = i;
                rProd.Name = RandomName();
                rProd.Price = _random.Next(1, 15);
                rProd.Description = RandomName();
                itemList.Insert(i, rProd);
            }
            foreach (var item in itemList)
            {
                Console.WriteLine("DEBUG "+item.Name +" "+item.Price);
            }
            return itemList;
        }
        public string RandomName()
        {
            Random _random = new Random();
            int length = 7;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(_random.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }

    }
}
