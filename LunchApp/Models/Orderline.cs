﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LunchApp.Models
{
    [Table("OrderLine")]
    public class Orderline
    {
        [Key]
        public int OrderlineID { get; set; }
        public int OrderID { get; set; }
        public int ProductID { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
