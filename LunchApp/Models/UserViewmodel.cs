﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace LunchApp.Models
{
    public class UserViewmodel
    {
        [DisplayName("User")]
        public string UserId { get; set; }
        public List<SelectListItem> ListOfUsers { get; set; }
    }
}
