﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchApp.Models
{
    public class CartItem
    {
        public int ItemId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public double price { get; set; }

        public CartItem()
        {

        }

        public CartItem(int itemId, string name, int quantity, double price)
        {
            ItemId = itemId;
            Name = name;
            Quantity = quantity;
            this.price = price;
        }
    }
}
