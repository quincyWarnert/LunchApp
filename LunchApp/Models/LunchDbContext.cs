﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchApp.Models
{
    public class LunchDbContext :DbContext 
    {
        public LunchDbContext(DbContextOptions<LunchDbContext> options):base(options)
        {

        }
       public DbSet<Orderline> Orderlines { get; set; }
       public DbSet<Orders> Orders { get; set; }
       public DbSet<Products> Products { get; set; }
       public DbSet<Users> Users { get; set; }
       public DbSet<ShoppingCart> ShoppingCarts { get; set; }
    }
}
