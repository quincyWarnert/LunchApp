﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace LunchApp.Models
{
    public class ProductViewModel
    {
        [DisplayName("Product")]
        public string UProductId { get; set; }
        public List<Products> ListOfProducts { get; set; }
    }
}
