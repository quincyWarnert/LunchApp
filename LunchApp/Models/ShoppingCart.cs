﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LunchApp.Models
{
  
    public class ShoppingCart
    { 
        private List<CartItem> Items { get; set; }
        public ShoppingCart()
        {
            Items = new List<CartItem>();
        }

        private int ItemIndexOf(int ID)
        {
            foreach (CartItem item in Items)
            {
                if (item.ItemId == ID)
                {
                    return Items.IndexOf(item);
                }
            }
            return -1;
        }

        public void Insert(CartItem item)
        {
            int _index = ItemIndexOf(item.ItemId);
            if (_index == -1)
            {
                Items.Add(item);
            }
            else 
            {
                Items[_index].Quantity++;
            }
        }


        public void DeleteItem(int rowId)
        {
            Items.RemoveAt(rowId);
        }

        public void Update(int rowId,int Quantity)
        {
            if (Quantity>0)
            {
                Items[rowId].Quantity = Quantity;
            }
            else
            {
                DeleteItem(rowId);
            }
        }

        public double TotalAmount
        {
            get
            {
                if (Items == null)
                {
                    return 0;
                }
                else
                {
                    double _totalAmount = 0;
                    foreach (CartItem item in Items)
                    {
                        _totalAmount += item.Quantity * item.price;
                    }
                    return _totalAmount;
                }
            }
        }


    }
}
