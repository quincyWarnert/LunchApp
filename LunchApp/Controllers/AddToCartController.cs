﻿using LunchApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace LunchApp.Controllers
{
    public class AddToCartController : Controller
    {
        ShoppingCart cart = new ShoppingCart();
        

        public IActionResult Add(int productId)
        {
            
            /*Geachte heer mevr,

                Kunt u mij laten weten wanneer u dit heeft afgehandeld.

                Groet Roy warnert
            
             bcc michiel
            */
            
            
                HttpContext.Session.SetString("Cart", JsonConvert.SerializeObject(cart));
                var myCart= HttpContext.Session.GetString("Cart");   

             if (myCart==null)
             {
                 List<Products> cart = new List<Products>();
                 var product = ctx.Tbl_Product.Find(productId);
                 cart.Add(new CartItem()
                 {
                    
                     Quantity = 1
                 });
                HttpContext.Session.SetString("Cart", JsonConvert.SerializeObject(cart));
            }
             else
             {
                 List<Item> cart = (List<Item>)Session["cart"];
                 var product = ctx.Tbl_Product.Find(productId);
                 cart.Add(new Item()
                 {
                     Product = product,
                     Quantity = 1
                 });
                 Session["cart"] = cart;
             }
            return Redirect("Index");
        }
    }
}
