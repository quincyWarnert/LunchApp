﻿using LunchApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunchApp.Controllers
{
    public class UserController : Controller
    {
        private LunchDbContext _lunchDbContext;
        private Users rUser = new Users();
        private Products rProduct = new Products();
        public UserController(LunchDbContext lunchDbContext)
        {
            _lunchDbContext = lunchDbContext;
        }
        public IActionResult Index()
        {
            var productList = rProduct.foodItems();
            var userList =rUser.GenerateUsers(); /*(from Users in _lunchDbContext.Users
                            select new SelectListItem()
                            {
                                Text = Users.FirstName,
                                Value = Users.UserID.ToString()
                            }).ToList();*/
            userList.Insert(0, new SelectListItem()
            {
                Text = "---Selected---",
                Value = string.Empty
            }) ;
            ViewBag.ListOfUsers = userList;
            ViewBag.ListOfProducts = productList;
            return View();
        }

        [HttpPost]
        public IActionResult Index(UserViewmodel userViewmodel)
        {
            var selectedValue = userViewmodel.UserId;
            return View(userViewmodel);
        
        }



    }
}
